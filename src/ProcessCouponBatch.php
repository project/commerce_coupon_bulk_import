<?php

namespace Drupal\commerce_coupon_bulk_import;

use Drupal\commerce_promotion\Entity\Coupon;
use Drupal\commerce_promotion\Entity\Promotion;
use Drupal\commerce_promotion\Entity\PromotionInterface;

/**
 * Batch class to process coupons.
 */
class ProcessCouponBatch {

  /**
   * Callback defined on the batch for process every order individually.
   *
   * @param array $chunk
   *   The rows of csv file.
   * @param string $promotionId
   *   The promotion to add this coupon to.
   * @param int $rows_count
   *   The quantity of the coupons for one import operation.
   * @param array $context
   *   The batch context.
   */
  public static function processCoupons(array $chunk, $promotionId, $rows_count, array &$context) {
    $promotion = Promotion::load($promotionId);
    foreach ($chunk as $coupon) {
      $couponCode = $coupon[0];
      $numberOfUses = !empty($coupon[1]) ? $coupon[1] : NULL;
      self::addCouponToPromotion($couponCode, $promotion, $numberOfUses);
      $context['message'] = t('Creating coupon: @coupon_code - Promotion ID: @promotion_id', [
        '@coupon_code' => $couponCode,
        '@promotion_id' => $promotionId,
      ]);

      $context['sandbox']['progress']++;
    }

    $promotion->save();

    $context['results'] = $rows_count;
  }

  /**
   * Add a coupon code to a promotion.
   *
   * @param string $couponCode
   *   The desired code of the coupon to be added.
   * @param \Drupal\commerce_promotion\Entity\PromotionInterface $promotion
   *   The promotion to add this coupon to.
   * @param int $numberOfUses
   *   The allowed number of uses for this coupon. Leave NULL for unlimited.
   */
  public static function addCouponToPromotion($couponCode, PromotionInterface $promotion, $numberOfUses = NULL) {
    // Create the coupon from the CSV file.
    $coupon = Coupon::create([
      'code' => $couponCode,
      'status' => TRUE,
      'promotion_id' => $promotion->id(),
      'usage_limit' => $numberOfUses,
    ]);
    $coupon->save();

    $promotion->addCoupon($coupon);
  }

  /**
   * Callback that check the end of the batch process.
   *
   * @param bool $success
   *   Boolean to flag if batch run was successful.
   * @param int $results
   *   Results of processed batch operations.
   * @param array $operations
   *   Operations ran in the batch job.
   */
  public static function processCouponsFinishedCallback(
    $success,
    $results,
    array $operations
  ) {
    // The 'success' parameter means no fatal PHP errors were detected. All
    // other error management should be handled using 'results'.
    if ($success) {
      $message = \Drupal::translation()->formatPlural(
        $results,
        'One coupon processed.', '@count coupons processed.'
      );
    }
    else {
      $message = t('Finished with an error.');
    }

    drupal_set_message($message);

  }

}
